AuthenticationFlowError = Java.type("org.keycloak.authentication.AuthenticationFlowError");


function authenticate(context) {
    var isVerified =  user ? user.getFirstAttribute("isUserEnabled") : "false";
    LOG.info(script.name + " trace auth for attribute: " + isVerified);

    if ("true" != isVerified) {
        
        context.form().setError("message.summary", ["afasdfasdfasdf"]).createForm("error.ftl");
        context.failure(AuthenticationFlowError.INVALID_USER);
        return;
    }

    context.success();
}
